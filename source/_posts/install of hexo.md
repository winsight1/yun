---
title: HEXO的安装
abbrlink: d77e65f8
tags: hexo
---



比较混乱，用来提示自己的

[主题配置 | Hexo-Theme-Yun (yunyoujun.cn)](https://yun.yunyoujun.cn/guide/config.html#内容卡片)



# 总体hexo安装

在终端中输入以下命令：

```sh
npm install hexo-cli -g
```

`install` 自然是安装。
`hexo-cli` 则是 `hexo` 的终端工具，可以帮助你生成一些模版文件，之后再用到。
`-g` 代表的是全局安装。也就是在任何地方都可以使用，否则会只能在安装的目录下使用。

接下来输入：

```sh
hexo init 你的名字.github.io
```

```sh
# 进入你的博客文件夹
cd 你的名字.github.io
# 默认安装所有 `package.json` 文件中提到的包
npm install
# 你也可以缩写成 hexo s
hexo server
```

`server` 代表开启本地的 Hexo 服务器，这时你就可以打开浏览器，在地址栏中输入 `localhost:4000` 就可以看到本地的网页了。

按 `Ctrl + C` 中断服务器的运行。

至此，基础的模版页面便已经搭建好了。

### **使用 Hexo 主题**

Hexo 默认提供的是 [hexo-theme-landscape](https://github.com/hexojs/hexo-theme-landscape) 主题。
默认主题样式简单，功能较少。所以大多数人并不会使用默认主题。

这里将示范如何使用我自己开发的主题 [hexo-theme-yun](https://github.com/YunYouJun/hexo-theme-yun)。

**下载 Hexo 主题**

进入终端（确保路径处于你此前使用 Hexo 初始化好的文件夹目录下，即 `xxx.github.io`，后简称为 `Hexo 目录`），输入以下命令。

> 实际上你也可以直接在 VS Code 中使用终端。

```sh
git clone https://github.com/YunYouJun/hexo-theme-yun themes/yun
```



> 这里便使用到了我们此前安装的 Git，`git clone` 即代表克隆（也就是复制的作用）我的主题（托管于 GitHub，链接便是主题所在的地址），`themes/yun` 则代表放在你 Hexo 文件夹下的 `themes/yun` 文件夹里（没有该文件夹会自动新建）。

### **生成静态文件和本地测试**

```
hexo g && hexo s
```

```
localhost:4000
```



# 一些功能用法



### 转载の链接

**[#](https://yun.yunyoujun.cn/guide/config.html#type)type**

为文章设置 `type` 属性，即可将其转为其他类型卡片，并跳转 `url` 设置的链接。

譬如：

```md
---
title: xxx
type: bilibili
url: https://www.bilibili.com/video/av8153395/
---
```

在文章标题前将会出现 bilibili 的图标，点击标题会跳转至对应的链接。

目前默认支持以下类型（哔哩哔哩、豆瓣、GitHub、网易云音乐、推特、微信公众号、微博、语雀、知乎、Notion、外链）：

```yaml
types:
  link:
    color: blue
    icon: icon-external-link-line
  bilibili:
    color: "#FF8EB3"
    icon: icon-bilibili-line
  douban:
    color: "#007722"
    icon: icon-douban-line
  github:
    color: black
    icon: icon-github-line
  netease-cloud-music:
    color: "#C10D0C"
    icon: icon-netease-cloud-music-line
  notion:
    color: black
    icon: icon-notion
  twitter:
    color: "#1da1f2"
    icon: icon-twitter-line
  wechat:
    color: "#1AAD19"
    icon: icon-wechat-2-line
  weibo:
    color: "#E6162D"
    icon: icon-weibo-line
  yuque:
    color: "#25b864"
    icon: icon-yuque
  zhihu:
    color: "#0084FF"
    icon: icon-zhihu-line
```



### 置顶

> 确保你的 [hexo-generator-index (opens new window)](https://github.com/hexojs/hexo-generator-index)为 `2.0.0` 或以上

通过设置文章 Front Matter 中 `sticky` 属性以进行置顶，数值越高，优先级越高。

设置置顶后，文章卡片右上角将出现置顶图标。

```md
---
title: xxx
sticky: 100
---
```

你也可以通过设置权重来实现多篇置顶文章的顺序。

```md
---
title: xxx
sticky: 1
---
---
title: xxx
sticky: 2
---
```

此时 `sticky: 2` 的文章将排列在 `sticky: 1` 的文章上面。



### 显示格式tab

{% tabs First unique name %}

<!-- tab -->

**This is Tab 1.**

<!-- endtab -->

<!-- tab -->

**This is Tab 2.**

<!-- endtab -->

<!-- tab -->

**This is Tab 3.**

<!-- endtab -->

{% endtabs %}



```c++
{% tabs First unique name %}

<!-- tab -->

**This is Tab 1.**

<!-- endtab -->

<!-- tab -->

**This is Tab 2.**

<!-- endtab -->

<!-- tab -->

**This is Tab 3.**

<!-- endtab -->

{% endtabs %}
```



# **hexo插件**

```python
npm install plug-in-name --save
```

打开 git，将当前目录切换到你的博客目录。使用 

```
npm ls -dept 0
```

即可查看，所有你**安装的 hexo 插件**。

### 使用图片绝对链接插件

```
hexo-asset-image
```

使用起来很简单，页面引入图片的方式也是md以前的语法

**安装**

```shell
npm install hexo-asset-image --save1
```

确保 `_config.yml` 中 `post_asset_folder: true`.

使用 `![logo](logo.jpg)` 就可引用到图片 `logo.jpg`.



### **aplayer**

**安装**

```
npm install --save hexo-tag-aplayer
```

**依赖**

- APlayer.js > 1.8.0
- Meting.js > 1.1.1

**使用**

```
{% aplayer title author url [picture_url, narrow, autoplay, width:xxx, lrc:xxx] %}
```

**标签参数**

- `title` : 曲目标题
- `author`: 曲目作者
- `url`: 音乐文件 URL 地址
- `picture_url`: (可选) 音乐对应的图片地址
- `narrow`: （可选）播放器袖珍风格
- `autoplay`: (可选) 自动播放，移动端浏览器暂时不支持此功能
- `width:xxx`: (可选) 播放器宽度 (默认: 100%)
- `lrc:xxx`: （可选）歌词文件 URL 地址

当开启 Hexo 的 [文章资源文件夹](https://hexo.io/zh-cn/docs/asset-folders.html#文章资源文件夹) 功能时，可以将图片、音乐文件、歌词文件放入与文章对应的资源文件夹中，然后直接引用：

```javascript
{% aplayer "Caffeine" "Jeff Williams" "caffeine.mp3" "picture.jpg" "lrc:caffeine.txt" %}
```

**歌词标签**

除了使用标签 `lrc` 选项来设定歌词，你也可以直接使用 `aplayerlrc` 标签来直接插入歌词文本在博客中：

```javascript
{% aplayerlrc "title" "author" "url" "autoplay" %}
[00:00.00]lrc here
{% endaplayerlrc %}
```

**播放列表**

```javascript
{% aplayerlist %}
{
    "narrow": false,                          // （可选）播放器袖珍风格
    "autoplay": true,                         // （可选) 自动播放，移动端浏览器暂时不支持此功能
    "mode": "random",                         // （可选）曲目循环类型，有 'random'（随机播放）, 'single' (单曲播放), 'circulation' (循环播放), 'order' (列表播放)， 默认：'circulation' 
    "showlrc": 3,                             // （可选）歌词显示配置项，可选项有：1,2,3
    "mutex": true,                            // （可选）该选项开启时，如果同页面有其他 aplayer 播放，该播放器会暂停
    "theme": "#e6d0b2",	                      // （可选）播放器风格色彩设置，默认：#b7daff
    "preload": "metadata",                    // （可选）音乐文件预载入模式，可选项： 'none' 'metadata' 'auto', 默认: 'auto'
    "listmaxheight": "513px",                 // (可选) 该播放列表的最大长度
    "music": [
        {
            "title": "CoCo",
            "author": "Jeff Williams",
            "url": "caffeine.mp3",
            "pic": "caffeine.jpeg",
            "lrc": "caffeine.txt"
        },
        {
            "title": "アイロニ",
            "author": "鹿乃",
            "url": "irony.mp3",
            "pic": "irony.jpg"
        }
    ]
}
{% endaplayerlist %}
```

**MeingJS 支持 (3.0 新功能)**

[MetingJS](https://github.com/metowolf/MetingJS) 是基于[Meting API](https://github.com/metowolf/Meting) 的 APlayer 衍生播放器，引入 MetingJS 后，播放器将支持对于 QQ音乐、网易云音乐、虾米、酷狗、百度等平台的音乐播放。

如果想在本插件中使用 MetingJS，请在 Hexo 配置文件 `_config.yml` 中设置：

```javascript
aplayer:
  meting: true
```

接着就可以通过 `{% meting ...%}` 在文章中使用 MetingJS 播放器了：

```javascript
<!-- 简单示例 (id, server, type)  -->
{% meting "60198" "netease" "playlist" %}

<!-- 进阶示例 -->
{% meting "60198" "netease" "playlist" "autoplay" "mutex:false" "listmaxheight:340px" "preload:none" "theme:#ad7a86"%}
```

有关 `{% meting %}` 的选项列表如下:

| 选项          | 默认值     | 描述                                                        |
| ------------- | ---------- | ----------------------------------------------------------- |
| id            | **必须值** | 歌曲 id / 播放列表 id / 相册 id / 搜索关键字                |
| server        | **必须值** | 音乐平台: `netease`, `tencent`, `kugou`, `xiami`, `baidu`   |
| type          | **必须值** | `song`, `playlist`, `album`, `search`, `artist`             |
| fixed         | `false`    | 开启固定模式                                                |
| mini          | `false`    | 开启迷你模式                                                |
| loop          | `all`      | 列表循环模式：`all`, `one`,`none`                           |
| order         | `list`     | 列表播放模式： `list`, `random`                             |
| volume        | 0.7        | 播放器音量                                                  |
| lrctype       | 0          | 歌词格式类型                                                |
| listfolded    | `false`    | 指定音乐播放列表是否折叠                                    |
| storagename   | `metingjs` | LocalStorage 中存储播放器设定的键名                         |
| autoplay      | `true`     | 自动播放，移动端浏览器暂时不支持此功能                      |
| mutex         | `true`     | 该选项开启时，如果同页面有其他 aplayer 播放，该播放器会暂停 |
| listmaxheight | `340px`    | 播放列表的最大长度                                          |
| preload       | `auto`     | 音乐文件预载入模式，可选项： `none`, `metadata`, `auto`     |
| theme         | `#ad7a86`  | 播放器风格色彩设置                                          |

关于如何设置自建的 Meting API 服务器地址，以及其他 MetingJS 配置，请参考章节[自定义配置](https://github.com/MoePlayer/hexo-tag-aplayer/blob/master/docs/README-zh_cn.md#自定义配置30-新功能)

### DPlayer

```
npm install hexo-tag-dplayer --save
```

**Usage**

```javascript
{% dplayer key=value ... %}
```

**key can be**

```
dplayer options:
    'autoplay', 'loop', 'screenshot', 'hotkey', 'mutex', 'dmunlimited' : bool options, use "yes" "y" "true" "1" "on" or just without value to enable
    'preload', 'theme', 'lang', 'logo', 'url', 'pic', 'thumbnails', 'vidtype', 'suburl', 'subtype', 'subbottom', 'subcolor', 'subcolor', 'id', 'api', 'token', 'addition', 'dmuser' : string arguments
    'volume', 'maximum' : number arguments
container options:
    'width', 'height' : string, used in container element style
other:
    'code' : value of this key will be append to script tag
```

**arguments to DPlayer options mapping:**

```
{
    container: "you needn't set this",
    autoplay: 'autoplay',
    theme: 'theme',
    loop: 'loop',
    lang: 'lang',
    screenshot: 'screenshot',
    hotkey: 'hotkey',
    preload: 'preload',
    logo: 'logo',
    volume: 'volume',
    mutex: 'mutex',
    video: {
        url: 'url',
        pic: 'pic',
        thumbnails: 'thumbnails',
        type: 'vidtype',
    },
    subtitle: {
        url: 'suburl',
        type: 'subtype',
        fontSize: 'subsize',
        bottom: 'subbottom',
        color: 'subcolor',
    },
    danmaku: {
        id: 'id',
        api: 'api',
        token: 'token',
        maximum: 'maximum',
        addition: ['addition'],
        user: 'dmuser',
        unlimited: 'dmunlimited',
    },
    icons: 'icons',
    contextmenu: 'menu',
}
```

see dplayer documents for more infomation.

**for example:**

```
{% dplayer "url=https://moeplayer.b0.upaiyun.com/dplayer/hikarunara.mp4" "addition=https://dplayer.daoapp.io/bilibili?aid=4157142" "api=https://api.prprpr.me/dplayer/" "pic=https://moeplayer.b0.upaiyun.com/dplayer/hikarunara.jpg" "id=9E2E3368B56CDBB4" "loop=yes" "theme=#FADFA3" "autoplay=false" "token=tokendemo" %}
{% dplayer 'url=some.mp4' "id=someid" "api=https://api.prprpr.me/dplayer/" "addition=/some.json" 'code=player.on("loadstart",function(){console.log("loadstart")})' "autoplay" %} 
```



# 文章加密

**在线演示**

- 点击 [Demo Page](https://mhexo.github.io/), **所有的密码都是 `hello`**.[hexo-blog-encrypt/ReadMe.zh.md at master · D0n9X1n/hexo-blog-encrypt (github.com)](https://github.com/D0n9X1n/hexo-blog-encrypt/blob/master/ReadMe.zh.md)

**安装**

- `npm install --save hexo-blog-encrypt`
- 或 `yarn add hexo-blog-encrypt` (需要) [Yarn](https://yarnpkg.com/en/))

**快速使用**

- 将 "password" 字段添加到您文章信息头就像这样.

```
---
title: Hello World
date: 2016-03-30 21:18:02
password: hello
---
```

- 再使用 `hexo clean && hexo g && hexo s` 在本地预览加密的文章.

**设置优先级**

文章信息头 > 按标签加密

**高级设置**

**文章信息头**

```
---
title: Hello World
tags:
- 作为日记加密
date: 2016-03-30 21:12:21
password: mikemessi
abstract: 有东西被加密了, 请输入密码查看.
message: 您好, 这里需要密码.
wrong_pass_message: 抱歉, 这个密码看着不太对, 请再试试.
wrong_hash_message: 抱歉, 这个文章不能被校验, 不过您还是能看看解密后的内容.
---
```

**`_config.yml`**

**对博文禁用 Tag 加密**

只需要将博文头部的 `password` 设置为 `""` 即可取消 Tag 加密.

Example:

```
---
title: Callback Test
date: 2019-12-21 11:54:07
tags:
    - A Tag should be encrypted
password: ""
---

Use a "" to diable tag encryption.
```