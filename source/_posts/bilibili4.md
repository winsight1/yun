---
title: 《一生所爱》最经典版本，二胡声起时，音符穿魂而过
type: bilibili
tags: 
  - 视频
  - bilibili
url: 'https://www.bilibili.com/video/BV1GZ4y1c7nR'
abbrlink: 3d4657b1
---

卢冠廷音乐作品品鉴会，经典再现大话西游《一生所爱》，二胡声起时，音符穿魂而过。

<!-- more -->