---
title: 《苏联亡党亡国20年祭》超清总集精编版
tags: 
  - 视频
  - bilibili
cover: 'http://i1.hdslb.com/bfs/archive/02efdbde6c20e31dab6f9391c36050c5fc381e1c.jpg'
movie: 苏联
abbrlink: a06812ca
date: 2021-05-14 12:59:45
---

<div class="bilibili">
<iframe src="//player.bilibili.com/player.html?aid=758078638&bvid=BV1o64y1277T&cid=336354649&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>
</div>
苏共亡党、苏联解体已过去数十年，但克里姆林宫上空红色的带有镰刀锤子图案的苏联国旗悄然落下的那令人叹息的一幕，却仿佛永远定格在眼帘。这是中国社科院世界社会主义研究中心制作的6集党员教育专题片《苏联亡党亡国20年祭——俄罗斯人在诉说》，沉痛的历史教训，前车之鉴不能忘！