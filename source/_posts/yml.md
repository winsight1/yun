---
title: 文章标头写法
categories: 项目案例
tags:
  - Hexo
  - blog
cover: 'https://cdn.jsdelivr.net/gh/duogongneng/MyBlogImg@master/imgQuietView.png'
abbrlink: 8cbccaa4
date: 2021-05-02 12:37:28
---

### 📖发布文章

你需要在发布文章的时候写标头



<!-- more -->

```
title: 一个简约扁平化的Hexo静态主题博客-Quiet
categories: 项目案例
tags:
  - Hexo
  - Quiet
  - 主题
  - 静态主题
excerpt: 采用简约大方的扁平化Hexo-Quiet主题
date: 2020-11-03 20:33:36
cover: 'https://cdn.jsdelivr.net/gh/duogongneng/MyBlogImg@master/imgQuietView.png'

```

**解释**

`title`：文章标题

`categories`：分类（最好只写一个）

`tags`：标签可以多个

`excerpt`：描述

`date`：创建日期

`cover`：缩略图（你不填就用默认的了）

