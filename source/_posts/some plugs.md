---
title: 一些插件的详细用法
abbrlink: ff28ec76
tags: 
  - 插件
  - hexo
---

# 音乐🎵



## 示例一(MetingJS播放器)

```javascript
{% meting "393688" "netease" "song" "theme:#C20C0C" %}
# 只更改 “” 内数字即可
# server必须值 音乐平台: `netease`, `tencent`, `kugou`, `xiami`, `baidu`
```

{% meting "393688" "netease" "song" "theme:#C20C0C" %}

## 示例二（本地资源加载\暂不可用）

```javascript
{% aplayer "汤家凤起床唤醒服务" "汤家凤" "tjf.mp3" "tjf.PNG" %}
```

{% aplayer "汤家凤起床唤醒服务" "汤家凤" "tjf.mp3" "tjf.PNG" %}



## 示例三：歌单

```javascript
{% meting "32040661" "netease" "playlist" %}
# 网易
```

{% meting "32040661" "netease" "playlist" %}

```javascript
{% meting "1628995233" "tencent" "playlist" %}
# tencent
```

{% meting "1628995233" "tencent" "playlist" %}

##### 进阶示例
```javascript
{% meting "2690544198" "tencent" "playlist" "autoplay" "mutex:false" "listmaxheight:340px" "preload:none" "theme:#ad7a86"%}
```

{% meting "2690544198" "tencent" "playlist" "autoplay" "mutex:false" "listmaxheight:340px" "preload:none" "theme:#ad7a86"%}

***[👉具体用法](https://winsight.github.io/posts/d77e65f8/)***

## 示例四

**直接插入**

```html
<iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=86 src="//music.163.com/outchain/player?type=2&id=393688&auto=1(控制自动播放)&height=66"></iframe>
```

<iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=86 src="//music.163.com/outchain/player?type=2&id=393688&auto=0&height=66"></iframe>

## 示例五

```c++
#音乐块，标签分别为“歌名”“作者”“音乐mp3链接”“封面链接”
{% aplayer "Caffeine" "Jeff Williams" "http://home.ustc.edu.cn/~mmmwhy/%d6%dc%bd%dc%c2%d7%20-%20%cb%fd%b5%c4%bd%de%c3%ab.mp3" "http://home.ustc.edu.cn/~mmmwhy/jay.jpg" "lrc:geci.txt" %}
```

{% aplayer "Caffeine" "Jeff Williams" "http://home.ustc.edu.cn/~mmmwhy/%d6%dc%bd%dc%c2%d7%20-%20%cb%fd%b5%c4%bd%de%c3%ab.mp3" "http://home.ustc.edu.cn/~mmmwhy/jay.jpg" "lrc:geci.txt" %}

# 视频💻

### 示例一： 

```javascript
{% bilibili "aid:60016166" "quality:high" "danmaku" "allowfullscreen" %}
```


{% bilibili "aid:60016166" "quality:high" "danmaku" "allowfullscreen" %}

### 示例二：

```javascript
#哔哩哔哩视频，数字为aid号
{% bilibili 587852774 %}
```

<div class="bilibili">
    {% bilibili 587852774 %}
</div>

### 示例三（网络直链）

```javascript
{% dplayer "url=http://www.nenu.edu.cn/_upload/article/videos/03/5f/7c999eed42e3aadc413d7f851f0e/0f50b3eb-9285-41d2-ac4d-6cc363651aad_B.mp4" "pic=http://home.ustc.edu.cn/~mmmwhy/GEM.jpg" "autoplay=true" "preload=metadata" "hotkey=true" %} 
```

{% dplayer "url=http://www.nenu.edu.cn/_upload/article/videos/03/5f/7c999eed42e3aadc413d7f851f0e/0f50b3eb-9285-41d2-ac4d-6cc363651aad_B.mp4" "pic=http://home.ustc.edu.cn/~mmmwhy/GEM.jpg" "autoplay=0" "preload=metadata" "hotkey=true" %} 





### 示例四（本地资源）

```javascript
{% dplayer "url=fwz.mp4" "pic=http://home.ustc.edu.cn/~mmmwhy/GEM.jpg" "autoplay=true"  %}
```

{% dplayer "url=fwz.mp4" "pic=fwz.gif" "autoplay=0"  %}





### 示例五：

可以添加所有有直接链接的视频

```javascript
#可以添加所有有直接链接的视频
<iframe 
    width="800" 
    height="450" 
    src='https://player.youku.com/embed/XNDE5NjYzOTg0OA=='
    frameborder="0" 
    allowfullscreen>
</iframe>
```

<iframe 
    width="800" 
    height="450" 
    src='https://player.youku.com/embed/XNDE5NjYzOTg0OA=='
    frameborder="0" 
    allowfullscreen>
</iframe>
