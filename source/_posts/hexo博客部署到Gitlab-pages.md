---
title: hexo博客部署到Gitlab pages
abbrlink: ee98dc9f
---



# 安装和配置环境

本教程系统平台为win10 64位家庭中文版。

## 安装Git

从Git官网地址：https://www.git-scm.com/download/ 下载Git软件，按默认选项安装即可。

## 安装Node.js

从node.js官网：https://nodejs.org/en/ 下载Node.js软件，推荐下载稳定版本，然后按默认选项安装即可。安装完成后，打开cmd.exe，输入node -v，若能正常显示版本号，则说明安装成功。

## 安装Hexo、预览本地hexo博客

在本地新建一个目录如hexo_blog用来存储hexo博客源码，然后在该目录下打开cmd.exe或Git bash here，依次运行下列命令(一个指令运行完再运行下一个指令)：

```
npm install hexo-cli -g #安装hexo命令行工具
hexo init #下载hexo博客初始源码
npm install #安装npm
hexo s 或 hexo server #启动本地服务器,可预览本地hexo博客
1234
```

如果正常安装完成，在浏览器中访问：http://localhost:4000/ 就可以看到运行在本地服务器上的博客了。

**注意：**

如果你使用的是Microsoft Edge浏览器，访问http://localhost:4000/ 时可能会失败，原因是这个浏览器经常偷偷地自动打开使用代理服务器127.0.0.1。关闭后即可正常访问。还有一种可能是你的4000端口被占用了。

# 将hexo博客部署到Gitlab pages上

## 创建Gitlab pages仓库

首先在Gitlab上注册一个账号，或者直接用Github账号登录也可以。

Gitlab支持project page和user page两种page，只需要创建对应的仓库即可。如果你要创建一个project page，假设project name为blog，那么你的project URL为：https://gitlab.com/username/blog/ 。一旦这个project启用了GitLab pages，并构建了站点，站点访问url为：https://uesrname.gitlab.io/blog/ 。如果你要创建一个user page，假设此时project [name为john.gitlab.io](http://xn--namejohn-0g0m.gitlab.io/)(john为你的Gitlab账号名)，则你的project URL为：https://gitlab.com/john/john.gitlab.io 。一旦这个project启用了GitLab pages，并构建了站点，站点访问url为：[https://john.gitlab.io](https://john.gitlab.io/) 。

这里我们新建一个名为username.gitlab.io的仓库，username就是你的Gitlab账户名。

## Gitlab账号添加SSH-key

点击你的Gitlab账号右上角头像，选择settings，然后在左侧菜单选择SSH keys。然后检查一下你的本地SSH key的情况，在hexo博客源码目录下点击Git bash here，依次输入以下命令：

```c++
cd ~/.ssh
ls #此时会显示一些文件
mkdir key_backup
cp id_rsa* key_backup
rm id_rsa*
#以上三步为备份和移除原来的SSH key设置
123456
```

如果第一步cs ~/.ssh出现cd: /c/Users/1234/.ssh: No such file or directory，或者第二步ls后没有文件，则说明你的本地目前没有ssh key(或在windows下查看[c盘->用户->自己的用户名->.ssh]下是否有id_rsa、id_rsa.pub文件)，那么使用命令：

```c++
git config --global user.name "winsight"
git config --global user.email "wangss.ch@gmail.com"
# --global参数表示你这台机器上所有的Git仓库都会使用这个用户名和邮箱地址的配置来提交，当然也可以对某个仓库指定不同的用户名和Email地址
# 用户名和邮箱地址是本地git客户端的一个变量,每次commit都会用用户名和邮箱纪录,github的contributions统计就是按邮箱来统计的。
ssh-keygen -t rsa -C "wangss.ch@gmail.com" #生成新的key文件,邮箱地址填你的Github地址
12345
```

然后按三次enter生成一个新的SSH key。打开id_rsa.pub文件，将里面的所有内容添加到上面Gitlab账号settings中的SSH key中。

## hexo博客源码目录下添加.gitlab-ci.yml文件

使用Gitlab pages部署hexo博客和Github pages不同。在Github pages上部署博客，需要先在本地生成各种静态网页和文件，然后再推送到Github pages仓库上就可以直接访问了。使用Gitlab需要在服务器端完成生成和部署两个阶段，需要在本地的hexo博客源码目录下添加一个.gitlab-ci.yml文件用来指导服务器如何处理你提交的源文件。

最新的.gitlab-ci.yml文件官方版本可以从这个仓库中获取：https://gitlab.com/pages/hexo/blob/master/.gitlab-ci.yml 。

.gitlab-ci.yml内容如下：

```
image: node:8.11.2

pages:
  cache:
    paths:
    - node_modules/

  script:
  - npm install hexo-cli -g
  - npm install
  - hexo clean
  - hexo generate
  - hexo deploy
  artifacts:
    paths:
    - public
  only:
  - master
123456789101112131415161718
```

## hexo博客本地源码推送到Gitlab pages仓库

在hexo博客源码目录点击鼠标右键Git bash here，依次运行下列命令：

```
git init
git add -A
git commit -m "init blog"
git remote add origin git@gitlab.com:username/username.gitlab.io.git
git push -u origin master
12345
```

这样我们就将hexo博客本地源码推送到Gitlab pages仓库上了。

### 可能存在的问题

Git 提示fatal: remote origin already exists 错误

最后找到解决办法如下：

1、先删除远程 Git 仓库

```
git remote rm origin
```

2、再添加远程 Git 仓库

```
git@gitlab.com:username/username.gitlab.io.git
git push -u origin master
```

## 开启Gitlab pages CI/CD生成Gitlab pages页面

上传后，然后Gitlab服务器会自动检查.gitlab-ci.yml脚本是否有效，校验通过后，会自动开始执行脚本。

点击左侧菜单中的CI/CD->pipeline可以查看脚本的执行情况，当脚本的Stages状态变为`test:passed && deploy: passed`时，说明构建完成。此时已可以访问我们的个人博客站点：[https://username.gitlab.io](https://username.gitlab.io/) 。有时构建完成时马上访问可能会出现404页面，这种情况很多人遇到过，这里是该问题的讨论：https://forum.gitlab.com/t/gitlab-pages-404-for-even-the-simplest-setup/5870 。其实这是因为Gitlab的服务器构建速度比较慢，等5-10分钟再重新访问页面就正常了。

# 总结

使用Gitlab pages部署hexo博客时，我们不需要在本地使用hexo generate命令生成博客静态网页，再push到Gitlab pages仓库，而是直接push了hexo博客的源码到Gitlab pages仓库，同时增加一个.gitlab-ci.yml文件作为CI/CD脚本，通过该文件在Gitlab服务器生成博客的静态网页，然后自动发布到Gitlab pages博客站点上。

当我们要在博客上写新文章时，只需把Gitlab pages仓库中的源码pull下来，然后使用hexo新建文章，使用markdown编辑器(如typora)编辑文章，完成后将源码再push到Gitlab pages仓库中即可，Gitlab服务器会根据.gitlab-ci.yml文件重新生成博客的静态网页，然后自动发布到Gitlab pages博客站点上。我们可以点击CD/CI configuration让Gitlab服务器自动检测.gitlab-ci.yml文件，若文件正确则自动运行和发布；也可以在Gitlab左侧菜单CI/CD->Schedules中添加new schedule，这样Gitlab服务器会定时重新运行.gitlab-ci.yml文件来重新发布博客。

