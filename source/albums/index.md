---
type: albums
albums:
  - caption: 夕阳西下
    url: /albums/sunset.html
    cover: https://i.picsum.photos/id/566/510/300.jpg?hmac=vxfbCcUU3XZCnwaRplLPPE3dQbHJkwURBsEPTbS8rVU
    desc: 我想起那天夕阳下的奔跑
---

